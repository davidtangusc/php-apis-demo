<?php

class SoundCloud {
	public $userID;
	protected $_client_id;

	const ENDPOINT = 'http://api.soundcloud.com/';

	protected function _curl($url)
	{
		return file_get_contents($url);
	}

	protected function _buildRequestURL($resource, $qs = array())
	{
		$qs['client_id'] = $this->_client_id;
		return self::ENDPOINT . $resource . '?' . http_build_query($qs);
	}

	public function __construct(array $options)
	{
		$this->_client_id = $options['client_id'];
	}

	/**
	 * [resolve description]
	 * i.e. http://api.soundcloud.com/resolve.json?url=http://soundcloud.com/thearmpit&client_id=fab1869f594107d250ff7afcd029dfc4
	 * @return [type] [description]
	 */
	public function resolve($profile_url)
	{

		$url = $this->_buildRequestURL('resolve.json', array(
			'url' => $profile_url
		));

		$json = $this->_curl($url);
		$jsonObj = json_decode($json);
		$this->userID = $jsonObj->id;
		
		// fetch user id from above and store in db
		return $this;
	}

	/**
	 * [getUsers description]
	 * i.e. http://api.soundcloud.com/users/2289277/tracks.json?client_id=fab1869f594107d250ff7afcd029dfc4
	 * @param  [type] $sub_resource [description]
	 * @return [type]               [description]
	 */
	public function getUsers($sub_resource, $qs = array())
	{
		$url = $this->_buildRequestURL($sub_resource, $qs);
		// dd($url);
		$json = $this->_curl($url);
		
		return $json;
	}


}